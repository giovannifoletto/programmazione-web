# HTML: Hypertext Markup Language

The HTML language is the standard language for creating Web Pages. The document created with this are standard for all network connected devices and works with every platform.

The **hypertext** is text displayed on a computer display or other electronic devices with references (**hyperlinks**) to other text that the reader can immediately access.

Hypertext documents are interconnected by hyperlinks, which are typically activated by user actions, such as a mouse click, keypress set or by touching the screen.

HTML 2.0 published as standard with `RFC 1866`. The initial development was donw under the authority of the W3C.

The two main principles taken in consideration are:

- Graceful degradation of presentation (automatic fallback for different fragment)
- HTML should convey the structure of a hypertext document, but not the details of its presentation.

After a while HTML evolved from a semantic language into a presentation language.

The initial maintenance group was W3C, after 2012 becomes the WHATWG (Web Hypertext Application Technology Working Group), that from 2019 is the only maintainer.

Initially in HTML1, the HTML code contains all the information for the style. After HTML 4.01 the HTML code contains only the structure, the "aspect of contents" is contained in the CSS code.

The main structure of a HTML documents is:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>My First html Page</title>
    </head>
    <body>
        <p>Welcome in my first page</p>
    </body>
</html>
```

HTML ignore indentations, all can be mapped in a single line (not good for writing it). That is done when minimizing the pages. The standard is **case insensitive**, although is preferred lowercase. HTML don't need a server to be displayed.

The `Doctype declaration` is important to understand what structure the document will have. Usually there can be added the W3C dtd file, but is not necessary.
HTML is created to work all the times, so it fallback to the most probably HTML element when is it the case.

The HTML document can be retrieved every time from the browser if it is visible, using the `see page source` option in the browser.

Best resources: [INTRO TO HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML).

## The `<head>` tag

```html
<head>
    <meta charset="utf-8">
    <title>My First HTML page</title>
</head>
```

The `HEAD` tag contains information (*metadata*) about the document. These can be:

- `<meta>`: used to describe the content (char-set, author, keywords for search engines, decriptions)
- `<title>`: is the title that appear at the top of the page in the browser window
- varius statement referencing JavaScript or StyleSheets files/code.

The charset information is useful to display the HTML page correctly, this way the web-browser can know how to correctly represent the information present in the page.
The possible mappings are between chars and integer numbers:

- **ASCII**: this specification was the first characters set, representing 128 different alphanumeric characters. It contains numbers (0-9), English Letters (A-Z) and some special characters (`!, $, +, -, ...`)
- **ISO-8859-1**: default charset for HTML4. This contains the code necesary to represent 256 different values.
- **ANSI** (Windows-1252): the orinal Windows charset. Add 32 extra characters to the ISO-8859-1.
- **UTF-8** (Unicode): is supported in HTML4 and the default in HTML5. 

The HTML has a problem with *entity names*, because they can be use symbols necessary to represent the structure of the HTML document itself (like `<>` for example). To resolve this problem is necessary to use HTML escaped elements. That table can be found [here](https://mateam.net/html-escape-characters/).

Another problem is the problem of encoding the URL of the page, because every URL can only contains a subset of characters. To resolve that problem are used this substitute chars ([link](https://www.w3schools.com/tags/ref_urlencode.asp)).

Another element really important in the `<meta>` tag is the *viewport*. That information set the user's visible area of the web page. The viewport should varies based on the devices, to abtain that a method is introduced in the HTML5 specification. 

```html5
<meta name="vieport" content="width=device-width, initial-scale=1.0">
```

With that the content of the page takes the correct width of the screen, and readapt itself to contains all the data in the best way possible and take the right level of zoom of the image. The readability is increased, clearly.

## The `<body>` tag

```html5
<body>
    <p>That the content of my page<p>
</body>
```

The `<body>` contains the actual content of the document. This is the part that will displayed in the browser windows.

## Other tags

To know HTML is essentially knowing a big set of tags.
To be remember: HTML is fault tolerant, so the concept of correctness is not get strictly, but the HTML viewer can solve a lot of inconsistency problems or other that will caused a problem in a normal `xml` document.

It is possible to nest tags.

There is the possibility of using *attributes* to insert additional information about the elements. Attributes are always specified in the start tag. Usually they move together in a *key-value* pair, inserted in the document like: `<tag attribute="value"></tag>`.
It is possible to add multiple attributes within a single tag, and is possible that some attributes do not get a associated value. All this value are limited to a string of 1024 chars and are contains in single o double quotes.

The basic formatting tags are: 

- for header `<h1>` to `<h6`
- for emphasis `<em>, <strong>, </b>, <i>`
- superscript or subscript `<sub>`, `<sup>`

The basic formatting tags for lists: `<ol>`, `<ul>`, `<li>`, `<dt>`, `<dd>`
That can be used to obtain **Ordered Lists** (`<ol>`), **Unordered lists** (`<ul>`), **Description lists** (`<dl>`
).

Every HTML element has a default *display value*, that can be `block` or `inline`.
The *block-level elements* are always at the beginning of a new line, and the browser automatically add some space (margin) before and after a element. These elements always take up the full width available (stretches out to the left and right as far as they can).
The *inline elements* do not start on a new line and take up only the width necessary.

To define paragraphs and blocks are used: `<p>`, `<div>`, `<span>`.
The `<p>` and the `<div>` are block elements with *semantic* differences:

- the `<div>` describes a container of data (with graphical separation)
- the `<p>` describes a paragraph of content (with graphic separation). This tag cannot contains block-level elements (like other `<p>` or a `<div>`) but can contain `<span>`. That said, to be remember that HTML is tolerant.

### Images and Figures

According to the W3C the *figure* element represent a unit itself, that has to contains all the information in itself. Can have a caption. The figure can be moved await from the main document without affecting in anything the original document meaning.

There are two tags useful to be used in the HTML documents to contains a figure: `<image>` (is used to embed images in a HTML document) or `<figure>` (used to semantically organize the content of an image in the HTML document).

### Hyperlinks

The hyperlinks are tags that allow to jump to another document with a click. Are represented as this: `<a href="url">link text</a>`. 

In that tag the `href` represent the URL that the hyperlink points to. Links are not restricted to HTTP-based URLs, but use that scheme to send to different types of resources.
In addition to that there is the possibility of adding a link to:

- fragments of a page with *URL fragments* (adding a `#` at the end of the link).
- the `media` fragments
- telephones numbers with `tel` URLs
- email addresses with `mailto` URLs

Examples:

```html5
<p>You can reach me at:</p>
<ul>
    <li><a href="https://mywebsite_gv.com"> My website</a></li>
    <li><a href="mailto:giovanna.varni@unitn.it">My email</a></li>
    \<li><a href="tel:+123456789"> My phone</a></li>
</ul>
```

Internal hyperlinks can be achieved with `#` (in facts they are HTML fragments).

```html5
<a href="#lesson1">Lesson 1</a></br> <!-- get to the 'Lesson 1' paragraph, with the ID=lesson1 -->

<h1 id="lesson1">Link arrive here</h1>
```

### Tables

Some examples:

```html5
<table>
    <tr>
        <td> Cell 1 </td>
        <td> Cell 2 </td>
    </tr>
</table>

<table>
    <tr>
        <td> Cell 1 </td>
    </tr>
    <tr>
        <td> Cell 2 </td>
    </tr>
</table>
```

To obtain a cell that is larger, use the *attribute*: `colspan=2`.


## File Location

A file path describes the location of a file in a website's folder structure. There are frequently-used links to external files, like:

- other Web Pages
- Images
- Style sheets
- JavaScript

There are more than one ways to organize the paths of resources. The rules are the same of a folder structure. Usually is not a great idea to have the absolute path for consistency reason. In other cases is useful to reprenset evey path starting from the "root" folder `/`.
Usually is considered a *best practice* to use relative paths (if possible).

Some examples:

```html5
<img src="../picture.jpg">
<img src="/images/picture.jpg"> <!-- NOT A BEST PRACTICE -->
<img src="images/picture.jpg">
<img src="picture.jpg">
<img src=”http://machine.domain/images/picture.jpg">
```

## Form

A form enables to collect (in an interactive way) information from the user. The informations collected are the passed to the backend application, such a CGI, PHP script, and so on that will handle the data according to the logic defined within the application.

The informations to submit/collect are through text input field, radio button, check-box and so on (all the `<input>` elements).



The form can have soThis new strategies is a little bit differentme attributes:

```html 
<form action="Script URL" method="HTTP Methd">
    form elements.
</form>
```

- `action`: the URL that identifies the resource processing the form submission
- `method`: the HTTP method to submit the form with. The most common are `GET|POST|PUT|DELETE`. The complete list is [HERE](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods).
- `autocomplete`: specifies whether a form schould have auto complete on or off. When auto-complete is on, the browser automatically complete values based on values that the user has entered before.
- `enctype`: specifies the *MIME* type used to send data to the server. It can be used only when the method is `POST`.
- *form elements*: these can be `<input>`, `<label`, `<textarea`, `<button>`, `<select>`, `<fieldset>`, `<legend>`, `<datalist>`, `<option>` 
  - The `<input>` type is different from the other, because it can use some specification and accept different type of values (complete [list](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)).
  - the  `<input type="hidden">` is used by developers to send to the server additional data which are not set in the form by users. 

The difference between `<button>` and `<input>`:

- there are 3 types of both types:
  - `type="submit"`: submits all input elements in a form to the server
  - `type="reset"`: reset all input elements in a form to its original values
  - `type="button"`: standard button that usually invokes a Javascript function.
- the main differences are that `<input>` buttons can only have text, whereas `<button>` can contain text, images and so on.



In HTML4 the only button that interact with the server is the `type="submit">` one. In HTML5, every buttons can possibly interact with the server.

The HTML5's `formaction` attribute (example [here](github.com/giovannifoletto/prog-web/test/formaction.html)):

- overrider the form action attributes for the element `<button>` and `<input>` that contains `type="submit"`.
- specifies where to send the data when a form is submitted
- gives us the ability to define as many buttons as we need and each of buttons can point different url.

 ```html
 <form method="GET">
     <label for="fname">First Name</label>
     <input type="text" id="fname" name="fname"><br>
     <label for="lname">Last Name:</label>
     <input type="text" id="lname" name="lname"><br>
     <button type="submit" formaction="/action_page.php">
         Submit
     </button>
     <button type="submit" formaction="/action_page2.php" method>
         Submit to Another Page
     </button>
 </form>
 ```

*TO BE NOTED*: the `formaction` has not the `method` attributes, so the only method possible is the `GET`.



# CSS: Cascading Styles Sheets

The original intern of `SGML`  (*[Standard Generalized Markup Language](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwiUgfbgsMD_AhXJgv0HHacpAywQFnoECA0QAQ&url=https%3A%2F%2Fit.wikipedia.org%2Fwiki%2FStandard_Generalized_Markup_Language&usg=AOvVaw24rhmDWZMoJGaiFbxsaWGW)*, the standard of the markup language all together, that contains `HTML`).

`HTML` was originally conceived for following this intent, but in reality (since `HTML3.2`) it degenerated into a markup language for format. That concept is a nightmare for developers. The solutions comes with separations of interest, introducing `CSS` (cascading style sheets, from the **W3C**). The concept is to define all the characteristic that a element can have, extracting it with the possibility of being modified with this language.

With that, the `<style>` tag can be used to modify the result of the markup:

```html
<h1 style="color:blue;text-align:center;">
    This is a heading H1
</h1>
```

That is an **inline CSS style**.

There are methods to generalize the style, like having all the CSS in a specific place, where all the object can reach and, with that, get all the same style (**internal CSS style**):

```html
<!DOCTYPE html>
<html>
    <head>
        <style>
            <!-- Define the style of all "h1" tags and all "p" tags. -->
            h1 {color: blue;}
            p {color: red;}
        </style>
    </head>
    <body>
        <tags></tags>
    </body>
</html>
```

The third and last method to get the style-sheet is to externalize the file (**external CSS style**):

```html
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="mystyle.css">
    </head>
    <body>
        <tags></tags>
    </body>
</html>
```



The CSS is an a language that can perform really well, but the complexity increase more and more the design is complex. The things that CSS can accomplish are beautiful and have great performance: [LINK](http://www.csszengarden.com).

## Formatting Elements

Let's introduce some formatting elements.



### Lenghts

There are two types of lengths used in CSS: *absolute* and *relative*.

The **absolute** units are considered to always be the same size:

| UNIT (SI/IS) | Name        | Equivalent          |
| ------------ | ----------- | ------------------- |
| cm           | centimeters | 1cm = 37.8px        |
| mm           | millimeters |                     |
| in           | inches      | 1in = 2.54cm = 96px |
| pc           | picas       | 1pc = 1/th of 1in   |
| pt           | points      | 1pt=1/72th of 1in   |
| px           | pixels      | 1px = 1/96th of 1in |

The **relative** length units are relative to something else. The benefit in using them is that we can make it so the size of something scales relative to everything else on the page.

| UNIT | Relative to                                  |
| ---- | -------------------------------------------- |
| em   | the font-size of the element                 |
| ex   | the x-height of the current font             |
| rem  | font-size of the root element                |
| vw   | relative to 1% of the width of the viewport  |
| vh   | relative to 1% of the height of the viewport |



### Colors

Colors can be specified using color name or color values.

- color *names*: 140 standard color names from `aliceblue` to `yellowgreen`.
- color value, expressed with:
  - `RGB`: `rgb(red, green, blue)`
  - `GRBA`: `rgba(red, green, blue, alpha)`
  - `HEX`: `#RRGGBB`
  - `HEX with transparency`: `#RRGGBB` with additional digits between `00` and `FF`.



We can apply colors to text, background, borders and other components that are displayed in the html document.



### Fonts

There are five generic font families:

- `Serif`: usually *Times Roman*
- `Sans Serif`: usually *Arial*
- `monospace`: usually *Courier*
- `script`: usually *Edurardian*, stile calligrafico
- `fantasy`: like *Sand*, stile decorativo

But you can really add whatever font you like, with `@font-face` css clause.

```css
@font-face{
    font-family: 'fontName';
    src:
        local('fontName'),
        url('fonts/fontName.otf'),
        format('opentype');
}
```

The font usually comes with *font size*. This option, can have different possibilities: `absoluteSize` or `relativeSize` or `percentage` or `length`.  Where:

- `absoluteSize`: have some possibilities `xx-small | x-small | small | medium | large | x-large | xx-large`.
- `relativeSize`: `lerger | smaller`

This components have two other important style:

- `font-style`: `normal | italic`
- `font-weight`: `bold | bolder | lighter | normal | 100 | 200 |.. | 900`



### Text

The `text` can be styled with:

- `text-align`: `left | center | right | justify`
- `text-decoration`: `line-throught | overline | underline`
- `text-indent`: `length | percentage`
- `text-transform`: `none | capitalize | uppercase | lowercase`



### Background

The `background` can be styled with:

- `background-color`: `color`
- `background-image`: `url("image src")`



### The Box Model

To simplify the construction of things in the page, every components use the *box model*. This model take every element as a box, that wrap itself and that can have some characteristic. 

This model define this different components:

![image-20230613172011334](./images/image-20230613172011334.png)

The total element *width* come now from the sum of *width*, *left padding*, *right padding*, *left and right borders*, *right margin*.

These boxes cane be formatted defining some characteristic:

- `padding`: for *bottom, top, right and left*, with `absolute |relative | percentage`.
- `padding`: `absolute | relative | percentage`

Then the divider can be styled too:

- `border-color`:  `color`
- `border-style`: `dotted | dashed | solid | double | groove | none`.
- `border-width`: `thin | medium | thick | absolute | relative`.
- `margin-*`: `auto | absolute | relative | percentage`
- `margin`: `auto | absolute | relative | percentage`



### Lists

There are 3 types of style for lists:

- `line-style-type`: 
  - `decimal | lower-alpha | upper-alpha | lower-roman | upper-roman`
  - `circle | disc | square`
- `line-style-image`: `url("src")`



## Selectors

To avoid repeat a lot some styles, some components can be grouped with selectors, that have the effect to take HTML elements based on some propositions:

- *basic*
- *combinator*
- *pseudo-class*
- *pseudo-element*
- *attribute*

### Basic Selectors

The basic selectors can be:

- *element* selector: select based on the tag type `p {style}`
- *id* selector: selection based upon id of some components `#component-id {style}`.
- *class* selector: selection is made with class `.class-name {style}`
- *universal* selector: get all the elements `* {style}`
- *grouping* selector: get a group of element `h1, h2, h3 {}`.



### Combinator Selectors

The basic selectors can be combined to obtain a more complex style system. The combinators can be:

- *simple + class*: ex. `p.red {style}`

- *descendant selector* `space`: ex. `div p {style}`.

  This combinator matches every elements that are descendant from a specific element. Like, in the example, the style is applied to every `<p>` elements inside a `<div>`.

- *child selector*: ex. `div > p {style}`.

  This combinator select every child of the first tag. The example matches every `<p>` that is a child of a `<div>`.

- *adjacent sibling selector* `+`: `div + p {style}`

- *general sibling selector* `~`: `div ~ p {style}`



Some test are done [HERE](github.com/giovannifoletto/prog-web/test/styling.html).



### Pseudo-class and Pseudo elements selectors

Pseudo-class selectors are used to define a special state of an element, like if the mouse is overing, if the links are visited or not, etc.

These classes are specified with `:`, like for examples `button:hover {style}`.

There are a lot of pseudo-classes, all visible [HERE](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes).



Pseudo-element selectors are used to style specified parts of an element, like the first letter, the first line, etc.

These pseudo-elements are specified with `::`, like for example `p::first-line {style}`. There are a lot of these too, all documented [HERE](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements).



### Attribute selectors

The attribute selectors are particular state of the tag that has to be differentiated by other. This selector is applied like `p[attribute=value]`. 

This classes can have more than one possible association:

- `[attribute]`: if an element has a `target` attribute
- `[attribute=value]`: if an element has an attribute like `target=value`
- `[attribute~=value]`: if an attribute contain the word `value`
- `[attribute|=value]`: select all element with an attribute starting or being `value`
- `[attribute^=value]`: get elements with attribute starting with `value`
- `[attribute$="value"]`: get elements with attribute ending with `value`
- `[attribute*=value]`: get elements with attribute containing a substring equals to `value`.



## Cascading and positioning

The style is applied as cascading, so only the last rule is applied. That means that is possible that some rules are not be applied, because there are some conflicts.

Some rules can help understanding what is maintained in a set of rule that collide with itself.

The steps that apply to the cascading order:

1. Selection of rules
2. Origin and importance
3. Specificity
4. Order of appearance

The origin of the style-sheet and the `!importante` clause are the first decision factors:

![image-20230613213636984](./images/image-20230613213636984.png)

There are some rules to follow, to understand what rules is chosen when displaying the content:

- **Specificity**: in case of equality with an origin, the specifity of a rule is the choice criterium. The declaration with the highest specificity wins.
- **Order of decreasing specifity**:
  - `Id selectors`
  - `Class selectors`
  - `Element selectors`
  - **DOES NOT** impact the specificity the *universal selectors* and the *combinator selectors*.
  - **Inline styles**: the inline style always overwrite any normal style in author style-sheets, so they have the highest specificity. The only way to override an inline style is to use the flag `!important`.
- **Order if appearance**: in the origin with precedence, if there are competing values for a property that are in style block matching selectors of equal specificity, th elast declaration in the style order is applied.



The **positioning** is the property used to set position for an element. Only after position is set, the *left*, *right*, *top* and *bottom* properties can be used. These properties behave differently, depending on the value of position.

There are several position values:

- **Static**: the default position of all the HTML elements. The are always positioned according to the normal flow of the page
- **Relative**: setting the *top*, *right*, *bottom* and *left* properties of a relatively-positioned element will cause it to be adjusted away from its normal position.
- **Absolute**: setting the *top*, *right*, *bottom* and *left* properties of a absolutely-positioned element will cause it to be adjusted in an absolute position with respect to its nearest positioned ancestor (if there is no ancestor, use the body)



Lastly, the CSS is capable of something amazing, like:

- pagination
- multiple colums
- tooltips
- graphic transformation (like 2D and 3D rotations)
- animations



# Web Servers

Web servers are programs of software that runs HTTP server software, which responds to requests from web browsers.

## Apache Web Server

The Apache Web server is a free and open-source cross-platform *web-server* software. The main release runs on some *linux distributions*. Is an HTTP server developed after 1995. Now it serves 25% of all internet traffic, with Nginx (the biggest competitor), 22%.

The software can upgrade its functionality with the use of some compiled modules, like *authentication*, or the support of some programming languages like *Perl*, *Python*, *Tcl*, *PHP*. Is often used as a *reverse proxy* or as *URL rewriting* components.



Usually, the Apache Software come pre-build in all linux systems. For the others systems are used software like **XAMPP**, that bundles a lot of different software:

1. **X**: Windows, Linux, Max, Anything (X)
2. **A**: Apache Web Server
3. **M**: MySQL, MariaDB (MySQL open source alternative, completely compatible)
4. **P**: PHP
5. **P**: Perl

The software come prebuild with a *control panel*, that allow the users to start/stop/restart all the services, or interact with one or another. This interface allow the read/write of some configuration files.

The most important directory in the whole filetree of this software is the **htdocs**. This folder contains all the programs for the web pages and the web pages itself.

The fact that the software is pre-build, permit to run directly `php` or `perl` code in the server simply making a file on the `htdocs` folder and open the page on a web-browser. If the database service is active, there is the possibility to access the database itself, run query and store data.



## Dynamic content: the server side programming

The main idea is to obtain *non static* information form the server for every page. This implies executing some code on it, and send the results to the user.

Originally, the web architecture used was file-based, so the file systems was used to create a collection of interlinked documents.

The evolution of server side programming language and interface is going really in the past. Let's see something.



### The first implementation: CGI

**Important sources**:

[WIKIPEDIA CGI](https://en.wikipedia.org/wiki/Common_Gateway_Interface)

[WIKIPEDIA FAST-CGI](https://en.wikipedia.org/wiki/FastCGI)

The **Common Gateway Interface** is a way to tell the server to spawn a process, get its results, that is it computes something and generate HTML pages, and send them as content of an HTTP Response.

This method was developed in the 1990s and was the earliest common method available that allowed a web pages to be interactive.

This type of script is became prevalent was the open source works done to it and the fact that was adopted as a standard in the Apache Web Server and then formalized in the `RFC 3875`.

The main use of this specification was initially run action on forms send. The interface to interface with this type of script is written in C, to permit to connect legacy information systems such a database to their web-servers. In that methods, action on users input became possible, with the interaction of external process.

Usually at the time the web-servers ((like Apache) display content in the `htdocs` directory, stored (thinking from a linux computer) in the `/usr/local/apache/htdocs`. This kind of documents are completely pre-written, and so **static**. The need of defining another path to get executable programs to run and create a page *on the fly* was needed. That directory is `/usr/local/apache/cgi-bin`.

The script that this directory can contains have access to the HTTP requests, and so they can elaborate the possible input to return a page differently as the inputs change. All programs that generate HTML pages dynamically are called **CGI-Script**. 

This programs are called *script* because are written in a *scripting language*.

In the early days, the only method to make a post request to the server was to include the `action` attribute in the `<form>` and then add a `<input type="submit">` in there. The `action` attribute indicate the URI of the script to be run. The page generated is pushed to the client after it has been generated.

The web server allows the owner of the service to configure which URLs shall be handled by CGI script, usually the `cgi-bin` directory. When a request arrive, there are some actions:

1. The HTTP daemon invoke another process to run the CGI-script.

2. The CGI-script is run and the output is collected by the server daemon. Some informations are passed to this script, and the CGI specification define it specifically.

   For example, the information about `PATH_INFO`, that contain the URL of the resources requesting the run of the script. Or, in case of a `HTTP_GET` request, the URL encoded possible payload in the `QUERY_STRING`. All this information are written in the `ENV` by the HTTP process, so are accessible with the `getenv()` system-call from the invoked-CGI-process.

3. The server (usually Apache) return the output of the script as a HTTP packets.



For convention, all the CGI script ends with `.cgi`, and the first line (like the Unix convention) set the scripting language chosen. 

A sum calculator CGI-script, for example can be:

```python
#!/usr/bin/env python3

import cgi, cgitb
cgitb.enable()

input_data = cgi.FieldStorage() # to request the input data to the CGI Api, written in C

print("Content-Type: text/html")
print("")
print("<h1>Addition Results</h1>")

try:
    num1 = int(input_data["num1"].value)
    num2 = int(input_data["num2"].value)
except:
    print("<output>Sorry, the script cannot runs if there are not numbers</output>")
    raise SystemExit(1)
 print(f"<output>{num1} + {num2} = {num1+num2}")
```



Like just sad, a lot of `ENV` are set in the web server environment, so the programs can make them accessible. The most used and standard are:

- Server specific variables:
  - `SERVER_SOFTWARE`: name/version of [HTTP server](https://en.wikipedia.org/wiki/HTTP_server).
  - `SERVER_NAME`: [host name](https://en.wikipedia.org/wiki/Host_name) of the server, may be [dot-decimal](https://en.wikipedia.org/wiki/Dot-decimal_notation) IP address.
  - `GATEWAY_INTERFACE`: CGI/version.
- Request specific variables:
  - `SERVER_PROTOCOL`: HTTP/version.
  - `SERVER_PORT`: [TCP port](https://en.wikipedia.org/wiki/TCP_port) (decimal).
  - `REQUEST_METHOD`: name of HTTP method (see above).
  - `PATH_INFO`: path suffix, if appended to URL after program name and a slash.
  - `PATH_TRANSLATED`: corresponding [full path](https://en.wikipedia.org/wiki/Full_path) as supposed by server, if `PATH_INFO` is present.
  - `SCRIPT_NAME`: relative path to the program, like `/cgi-bin/script.cgi`.
  - `QUERY_STRING`: the part of URL after the "[?](https://en.wikipedia.org/wiki/Question_mark)" character. The [query string](https://en.wikipedia.org/wiki/Query_string) may be composed of *name=value pairs separated with [ampersands](https://en.wikipedia.org/wiki/Ampersand) (such as var1=val1**&**var2=val2...) when used to submit [form](https://en.wikipedia.org/wiki/Form_(web)) data transferred via GET method as defined by HTML [application/x-www-form-urlencoded](https://en.wikipedia.org/wiki/Application/x-www-form-urlencoded).
  - `REMOTE_HOST`: host name of the client, unset if server did not perform such lookup.
  - `REMOTE_ADDR`: [IP address](https://en.wikipedia.org/wiki/IP_address) of the client (dot-decimal).
  - `AUTH_TYPE`: identification type, if applicable.
  - `REMOTE_USER` used for certain `AUTH_TYPE`s.
  - `REMOTE_IDENT`: see [ident](https://en.wikipedia.org/wiki/Ident_protocol), only if server performed such lookup.
  - `CONTENT_TYPE`: [Internet media type](https://en.wikipedia.org/wiki/Internet_media_type) of input data if PUT or POST method are used, as provided via HTTP header.
  - `CONTENT_LENGTH`: similarly, size of input data (decimal, in [octets](https://en.wikipedia.org/wiki/Octet_(computing))) if provided via HTTP header.
  - Variables passed by user agent (`HTTP_ACCEPT`, `HTTP_ACCEPT_LANGUAGE`, `HTTP_USER_AGENT`, `HTTP_COOKIE` and possibly others) contain values of corresponding [HTTP headers](https://en.wikipedia.org/wiki/HTTP_headers) and therefore have the same sense.



The CGI interface is used today, like in the `nodejs` server environment. That sad, other technologies emerges, like the **FAST-CGI**, used by Nginx or Caddy. This technologies was introduced in the mid 1190s. The concept that this new interface want to solve is the scalability: 

In a CGI server, for every request the web-server create, invoke and then destroy a process. That approach "one new process per request" makes CGI very simple to implement but limit the efficiency and scalability, in fact the operating system overhead, expecially in high-loads, becomes significant.

The *FAST-CGI* instead of creating new process for each request, uses persistent process to handle a series of request s. These processes are owned by the FastCGI server, not the web server.



### A simpler idea: a Template

Usually, a CGI-script, print the HTML source code out to the STDOUT. The web-server daemon get all this output and send them on the internet to the client.

The simpler idea behind that is, instead of embed  HTML into code, embed code in the HTML page.

That permit a simplification: all the code is really clear in the page and easier to maintain. Another point is on the logic behind that mechanism: all the HTML source to render is present and clear, the code is embedded inside them. That allows to create HTML really valid and complex, that will be difficult if there were the need of programmatically create it.

The newer server, after 1990s are capable of parsing a web page and executing code in it. The actions are:

1. Retrieve template page from file system
2. Evaluate all code snipped
3. Send Response



There are varius languages that implements this feature, in particular, for Windows there is **ISP**, for Java there's **JSP**, in general there is **PHP**.

Althought the PHP compiler is embedded in the XAMPP stack with Pearl, for Java there's the need of a new interpreter that works with the Tomcat web server.

#### PHP

An example of the approach of template is **PHP** (PHP hypertext processor), that in fact runs directly in HTML pages. The differences here are in the methods to get the environment variables. The "superglobals" are set by the HTTP process and passed only to the PHP interpreter, that granularly inject into the script at runtime.

PHP is a language like another, so has variables, logic, blocks and structure. All the php-code blocks are like that:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>PHP TEST</title>
    </head>
    <body>
        <h1>
            Text within the HTML.
        </h1>
        <?php
			echo "Text generate with php."
		?>
    </body>
</html>
```



The variables can be declared anywhere in the script. Variables can have different variable scopes: *local* (variable declared inside a function), *global* (variable declared outside a function), *static*.

There are two ways to access a variable from inside a function:

- using a keyword `global`
- using the associative array `$GLOBALS[key]`. In there, the index holds the name of the variable

The `GLOBALS[key]` is an example of **superglobal** variables, that are built-in variables always accessible (from any function, class, file) that is accessible in all scopes through a script.

The most used PHP superglobal variables are:

- `$GLOBALS`
- `$_SERVER`: this variables hold information about *headers*, *paths* and *script locations*
- `$_REQUEST`
- `$_POST`  and `$_GET`: contains the parameters passed in the client request
- `$_FILES`
- `$_ENV`
- `$_COOKIE`
- `$_SESSION`





