---
author: ["Giovanni Foletto"]
date: "2023-03-08"
version: "0.1-devel"
last-modification: "2023-03-08"
book: true
title: "Appunti di Introduzione alla programmazione Web"
teacher: "Giovanna Varni"
titlepage: true
---

# Introduction

The first thing to do is to define the difference of two concept:

- **INTERNET** is a global system of interconnected computer networks that interchange data by packet switching using the standardized Internet Protocol (*IP*). The Internet is a network of network that works with the TCP/IP standards.
- the **WEB** (World Wide Web) is an *information system* where documents and other web resources are identified by *Uniform Resources Locators* wich may be interlinked by hypertext and are accessible over the Internet.

The WEB uses a lot of technologies:

- *HTML* (HyperText Markup Language) to describe page look
- *HTTP* (HypetText Transport Protocol) to transfer informations
- a *Web Browsers* is a class of software build to access the WEB resources/app
- a *Web Server* is a type of software that can provide/publish document or information that has to be accessible throught the Internet.

The WEB was invented by *Tim Barners-Lee* at CERN in 1989-1990. The 30 April 1993, the CERN put the WEB software under a Open Source License. With that the technologies needed to access the WEB became available to the public domain. With that move, they maximize the dissemination of the use of that technology.

Tim Barners-Lee after have left CERN, become professor at the MIT of Boston, where in 1994 found the World Wide Web Consortium (W3C), an international community devoted to developing open web standard.

Now, the **W3C** (*World Wide Web Consortium* remain as the community as intended when founded. The Members organization, a full-time stadd and the public (business or single person) works together to develop *Web Standards*.

The evolution and standardization of the Internet is controlled by the **IETF** (*Internet Engineering Task Force*), a large open international community of network designers, operators, vendors and researchers with the goals the evolution of the internet architecture and the smooth operation of them.

The web programming is difficult in particular for some reason:
- a cerntral server has to handle multiple concurrent connection, with the possibilities of scaling to milions.
- multiple clients has to work smooth for the users, and has to work well for every user. 
- The server architecture is becoming more and more complex, with the use of microservice, containers and all the thing that derive from that technological advantage.

## W3C Standards

During the life of the *W3C* organization, a lot of technologies have become standard. With that methods of having public and open source standards, the web applications made public can interoperate without having difficulties in transaction, decodind and understand what is sent and how to work with that.

In this group are present standards for  Web Design and Applications(like web design, design of application, building and rendering Web Pages, languages or document needed for that like HTML, CSS, SVG, Ajax), Web of Devices(to enable accessibility to the internet from every possible devices connected on the Internet, even the automachine world), Web Architecture(the foundation technologies to build and work with the Web, inlcuding URI and HTTP), Semantic Web (web documents), XML Technology (all the XML standard), Web of Services (message-based design applied to the services in order to communicate in the enterprise software), Browser and Authoring Tools (to assure the interoperability and the control over the complete support of the other internet standards).

Under the Web Design and Applications are:

- HTML&CSS
- Javascript wb APIs
- Graphics
- Audio and Video
- Accessibility
- Internationalization
- Mobile Web
- Privacy
- Math on the Web

Under the Web Architecture section are:

- Architecture Principles
- Identifiers
- Protocols
- Meta Formats
- Protocol and Meta Format Considerations
- Internationalization

Under XML thecnology:

- XML essentials
- Efficient Interchange
- Schema
- Security
- Transformation
- Query
- Components
- Processing
- Internationalization

# Internet Standard

## Protocols

The **Protocol** is a code of behavior that delineates expectation and return values. 
The **Communications protocol** are a set of rules and regulation that determine how data are transmitted in telecommunications and computer networking.

Protocols usually defines:

- the *format* (the data that has to be present and how that are encoded and what they contains)
- the *order* of messages exchanged betweeen two or more communicating entities
- the *actions* taken on the transmission and/or receipt of a message or other event.

## Socket and Port

A process associates its input or output channels via an internal *Socket* with a transport protocol, a port number and an IP address. This process is known as **binding**.
Usually a socket is associated with the PID (process identifiers).
```
socket := {protocol, local address, local port, remote address, remote port}
```

## HTTP Overview

The hyperText Transfer Protocol (*HTTP*) is an application level protocol for distributed, collaborative, hypermedia information systems. Is 

continue on PDF2 - all get from the same book of networking. No more added.


## URI, URL, URN

A web resources, or simply a resource, is any identifiable thing, whether digital, physical or abstract.

The **Uniform Resources Identifier (URI)** is a compact sequence of characters that identifies an abstract or physical resource.

The **Uniform Resource Locator (URL)** refers to the subset of URI that identify resources via a representation of their primary access mechanism.

The **Uniform Resource Name (URN)** refers to the subset of URI that are required to remain globally unique and persistent even when the resources ceases to exists or become unavailable. It is intended to serve as persistent, location-indipendent, resource idetifier.

Both the URL and URN are in fact URI. We can think the URN as the identification for the resource and the URL the method for finding it. A URN can be associated to many URLs.

The URI standard (RFC 8141, that make obsoletes RFC 2141 and 3406) usually works like:
```
<scheme>:<scheme-specific-part>
```
The scheme could be: `http`, `https`, `ftp`, `mailto`, `geo`, `fax`, ... (more at [link](https://en.wikipedia.org/wiki/List_of_URI_schemes)).

The more complete *URI* standard is: `scheme:[//authority]path[?query][#fragment]`, where the authority part consists in `authority = [userinfo@]host[:port]`.

## Mime Type

The URI, URN and URL informations cannot provide some insite about what kind of data is being conteined at the resources. For that reason is used the MIME type standard. 

Using a fixed file extension is not the most correct ways of doing things, because there are ways to have an extension correct on a different type of file. See [Unix Magic Numbers](https://www.geeksforgeeks.org/working-with-magic-numbers-in-linux/).

The **MIME** (*Multipuroise Internet Mail Extensions*, RFC 2045 and 2046) type standard is useful to understand what type of payload is passed by in the packets.
The standard works with the rule `MEDIA_TYPE/SUBTYPE`, for example:

- type `text` with possible subtype `text/plain, texxt/html, text/richtext, ...`
- type `image` with possible subtype `image/jpeg, image/png, image/svg+xml, ...`
- type `video` with possible subtype `video/mp4, video/ogg, ...`
- type `application` with possible subtype `application/x-apple-diskimage, ...`
- type `multipart`

Other possible type present in this standard ara contained [here](https://www.freeformatter.com/mime-types-list.html).

## Client and Server Approach

The computer and other devices connected to the internet are often referred to as *end systems* (**hosts**).Hosts are sometimes further divided into two categories, *clients* and *servers*.

Usually, client tend to be desktop PC, laptop or mobile phone, servers are usually more powerful machines that store and distribuite Web pages, stream video, relay email and so on.

More from the pratical point of view, the server is a machine that opens a connection and waits for incoming calls(in order to provide a service). A client is a machine that starts a connection (aka opening a socket to the server) and requests a service.

Most used server on the web are Apache and Nginx.

## Web Architecture

There was an iniatial architecture, evolved in what is now the standard.

### Static Pages

At the beginning of the WEB era, all the pages are get from the server are static. All the links that are contained in there are fixed in the document that was sent. The mapping from URLs and Static Resources is 1:1. The web server is nothing but a retriver of the content associated to an URL (typically HTML page).

The technology used is directly dipendent to the performance. There are a lot od server programs solution now. 

### Dynamic Pages

The server programs inject information at the time of the request or update the links every time there is a new requests to them.

In this case, the URL are associated with *actions* and are a carrier for the action *parameters*.
The web server:

- understand that certain URLs are *dynamic*
- parses the parameters
- starts a process (or a thread) corresponding to the desired actoin
- performs some computations
- passes the results of computations to the client as HTML pages.

Usually with that comes the possibility to have a lot of different programming languages that do the same things.
The most used, from the server side prospective are *Java, Javascript, PHP*.

### Dynamic Pages with DB

The programs usually has to interoperate with a lot of data. Databases are used to save and retrieve specific data when they are needed. 

### Flash Player and Silverlight

To add some interactivity to the pages, Adobe creates a programs, called Adobe Flash Player. That is a proprietary code that can run on different browser. 
A similar solution is from Microsoft, that present the Silverlight project.

The two components have done a great work doing what they are ment to be doing, but some problem got in the way.
The software was slow, sometimes buggy and not completly working on phones. After 2008 there are more phones on the internet than ever. Iphone decided to not implement Adobe Flash player on them.
The last shot was the HTML5 standard, issued by the W3C the 22 january of 2008. That standard includes all the possibilities and integration that Adobe Flash Player was thought to do at the beginning. And done better without compromising performances.

All that put these plugin eventually to an end.

Other improvements to the technology are made after that, and faster than ever.

# Introduction to Markup Languages 

A markup language is a system form annotating a document in a way that is syntactically distinguishable from the text. 
The language specifies a code form formatting, bot the layout and style, within a text file. The code used to specify the formatting are called tags.
In most cases are human-readable.
A markup language is *NOT* a programming language.

## Key Teminology

**TAG**: a sequence of characters that begin with `<` and ends with `>`. That cames with 3 flavours:

- *Start Tag*: `<book>`
- *End Tag*: `</book>`
- *Empty-element Tag*: `<empty/>

**ELEMENT**: a sequence of characters that begin with a start-tags and ends with a matching end-tag, or consists only of a empty-element tag. The characters between the start and the end tag, if any, are the element's content and can contain other elements called children.

**ATTRIBUTE**: a name-value pair existing in a start-tag or in a empty element tag.

An example of a markup language can be:
```
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<data>
    <NETWORK>
        <IP>172.150.1.101</IP>
    </NETWORK>
    <LECTURE id="27">
        <COURSE_NAME>Web Programming</COURSE_NAME>
        <LECTURE_NAME>Introduction to XML</LECTURE_NAME>
        <TEACHER_NAME>Giovanna Varni</TEACHER_NAME>
        <TIME>5225.00</TIME>
    </LECTURE>
</data>
```

Markup languages are usually important for:

- storing, managing, delivering, structuring, publishing data
- makeking explicit to a machine what is implicit to a person
- assists us in the re-use of the same information:
    - in different formats
    - in different contexts
    - by different classes of users

There are more than one type of markup languages:
 > NON CHIARO, DA RISCRIVERE (TODO)

1. **Presentational Languages**: are use by traditional word-processing systems. Binary codes embedded within document text that produce the **WYSIWYG** ("What you see is what you get!"). Often this type of markup language is hidden from human users, even authors and editors. An example is the *Rich Text Format (RTF)* used internally from Word.
2. **Procedural Markup**: provide instructions for programs to process the text. The processor runs throught the text from beginning to end, following the instructions as encountered. Text with such markup is often edited with the marjup visible and directly manipulated by the authors. An example could be Markdown, where you can have the indication of the title, and only after a process to that properties is given the style as it has to get.
3. **Descriptive/Semantic Markup**: used to label parts of the document for what they are, rather than how they should be processed. The objectvie is to decouple the structure of the document from any particular treatment or rendition of it. Descriptive markup encourages authors to write in a way that describes the material conceptually, rather than visually. Examples like LaTex, HTML, XML.

## SGML: the roots of Markups Languages

SGML is an ISO standard (ISO 8879:1986) which provides a formal notation for the definition of generalized markup languages.
SGML is not a language in itself. Rather, it is a metalanguage that is used to define other languages.

A SGML document is the combination of three parts:

1. The *content* of the document (words, pictures, etc.). This is the part that the authors wants to expose to the client.
2. The *grammar* (**DTD**: *data type definition*) that defines the accepted syntax.
3. The *stylesheet* that establishes how the content that conforms to the grammar has to be rendered on the output device.

Generally, we refer to the parts as a unique files (they don't have to be separated in different physical files).

## XML: eXtensible Markup Language

XML, **eXtensible Markup Language** is a group of technologies created in 1998 from the W3C for web development.
XML is used in:

- Having data in a easy format
- Exchanging data between applications 
- Accessing to databases
- Changing to format of data
- Writing config files
- Web services
- and more!

XML is not meant for doing everything, is only thought to be an information wrapper.
The advantages of this language are that follow simple rules and explain everything in human readable documents.
The XML standard is platform indipendent and many tool are available for wrinting and validating docs.
With this language you can generate really complex documents that can be managed a simple text document when they travel the web.

The concept behind all that is the **vendor indipendence** in the data-formatting context. XML do no create depencies, so you can build solution that are completly agnostic about *platform* and *software*.

From XML are derived a lot of other languages: 

- XML, DTD, XSL, XSLT
- DOM, SAX, JAXP, JDOM
- XML Schema, XPath, XLink, XPointer
- XQL, XML-RPC, XSP
- Related stuff are SGML, XHTML, CSS

XML do not use predefined tags, it possible to define them as needed. The standard provide a grammar to define tags, define rules for the tags, allowed attributes, containment rules.

The grammar logic is defined ina a DTD file, XML-Schema file or is not defined at all.

For an XML file to be well formed has to comply some requirements:

- XML is **case sensitive**
- there must be a single top-level tag (called **root tag**) that contains all
- each tag must have a closing tag or, if empty, may use the abbreviated form (`<book/>`)
- tag must be appropriately nested, and follow the order of the respective start tags: `<b><i>Tex bold and italic</i></b>`
- attribute tags must be enclosed in single or double quotes. Use the correct double/single quotes to "escape" some possibilities.
```xml
<note date="12/11/2007">
    <to>Pippo</to>
    <from>Me</from>
</note>
```

- empty elements can contain attributes
- no markup characters are allowed. That means the data cannot contains `<`, `>`, `"`, `&`. If you need them, you have to use their escaped representation `&lt;`, `&#60`, `&#x3c`.

In XML there is not a rule about when to use an element or an attribute, so the two following example are the same:
First:
```
<person gender="famale">
    <name>Anna Smith</name>
</person>
```
Second:
```
<person>
    <gender>female</gender>
    <name>Anna Smith</name>
</person>
```

But there are some differencies: 
- attribute cannot contains multiple values (elements can)
- attribute cannot contain tree structures (elements can)

## XML: Tree structure

Every XML documents must have a root tag (a tag that contains all the others).
An XML document is an information unit that can be seen in two ways:

- As a linear sequence of characters that contains characters data and markup
- As an abstract data structure that is a tree of nodes

The **logical structure** of a document is:

- XML declaration (or "Prolog"). It's *optional*, but if present, it must be *the first element*. That would be: `<?xml version='1.0' encoding 'utf-8'>`
- Optional DTD declaration
- Optional comments and prcoessing Instructions (PIs)
- The root element's start tag
- All other elements, comments and PIs
- The root element closing tag

## Namespaces

Since you can define your own tags, if you reuse XML files from other authors you might find tag conflicts.
Conflicts can be avoided by declaring a *namespcae* as an attribute of the root element.

A namespace would be defined as:
```
<root xmlns:h="https://www.ikea.com/furniture" xmlns:f="http://www.gym4all.it/tnt/people">
<h:table>
    <h:name>Vanga</h:name>
</h:table>

<f:table>
    <f:name>Pippo</f:name>
</f:table
```

## Parsers

A parser in the context, is a software tool that preprocesses an XML document in some fashion, handling the results over to an application program.

The primary purpose of the parser is to do most of the hard work up front, in order to provide the application program with the XML informations in a form that is easier to work with.

The first phase of the parser is the check if the document is well-formed.

### Tree-based vs Event-based API

The XML API permit to access to the values and object of the XML document. There are two ways to do that: The Tree-based or the Event-based API.

The **Tree-based API** compiles an XML document into a internal tree structure. That makes possible for an application program to navigate the tree to achieve its objective. 
The Document Object Model (**DOM**) working group at W3C developed a standard tree-based API for XML.

The **Event-based API** reports parsing events (such as the stard and end of elements) to the application using *callbacks*. The application implements and registers event handlers for the different events. Code in the event handlers is designed to achieve the objectvie of the application. The process is similar to creating and registering event listeners in the *Event Model* by Java and other languages.

### DTD and XML - Schema

A well-formed XML document conforms to the rules (the grammar) of either a DTD or an XML-Schema is a valid XML document.

` TODO: DA RIVEDERE Questa cosa`
Despite that, validity is not a requirement of XML, in fact a invalid XML document can be a perfectly good and useful XML document. A non well-formed document cannot be valid and is not an XML document.

To conclude, the validation against DTD or XML-Schema can be often really useful, but is not required.

### DTD Definition

The DTD is a component of XML standard that define the **Data Type Definition**. It can be seen like a class blueprint for the document. It can contains all the information with the elements required and other informations.

A DTD can be:

- internal to a document `<!DOCTYPE rubrica [DTD Content]>`
- External to a document `<!DOCTYPE root_tag SYSTEM "FileDTD.dtd">` or `<!DOCTYPE root_tag PUBLIC "FPI" "URL">`

#### DTD ELEMENT

The DTD document can have constaints on subtags:

- `?` Zero or one element
- `+` One or more element
- `*` Zero or more element
- `,` sequence
- `|` or

#### DTD ATTLIST

### XML Schema

DTDs are complex and not written in XML, so there are other libraries to get in. XML Schemas or XML Schemas Definizion (XLS) in contrast are anothe W3C standard since 2001 and use standard XML to describe the components of the CML document.

Eaxh XML document defined throught a DTD can be also defined throught a XML Schema, the opposite is not necessarily true.

The `<schema>` element is the root element of every XML Schema. This `<schema>` tag declares the namespace `xs` (attribute `xmlns:xs`) whose tag are defined [here](http://www.w3.org/2001/XMLSchema).

XML Schema elements: 

- Simple elements: contains only text, not other elements or attibutes.
- Complex elements: can contain other element and/or attributes

There are four kinds of *Complex Elements*:

- empty elements
- elements that contain only other elements
- element that contain only text (and attributes)
- elements that contain both other elements and text

Simple elements are defined like: `<xs:element name="xxx" type="yyy" />`.
The most common built-in data types are:

- `xs:string`
- `xs:decimal`
- `xs:integer`
- `xs:boolean`
- `xs:date`
- `xs`time`

Simple elements may have a default value or a fixed value. In the second case, the value has to be present.

Attributes are defined like this: `<xs:element attribute="xxx" type="yyy"/>`.
The most common built-in data types are:

- `xs:string`
- `xs:decimal`
- `xs:integer`
- `xs:boolean`
- `xs:date`
- `xs:time`

As the simple elements, attributes may have default or fixed value.

*Complex element is a combination of what is above. They start with a `<xs:element name="student">` that contains all the other parts.
There is more than one possibility to describe a XML document. It may vary beacuse there are different type of structure and type that can be inserted in the document. 

If there are elements that contains other element can be done like:

```
<xs:element name="student">
    <xs:complexType>
        <xs:sequence>
            <xs:element name="firstname" type="xs:string"/>
            <xs:element name="lastname" type="xs:string"/>
        </xs:sequence>
    </xs:complexType>
</xs:element>
```
Or it can be:

```
<xs:element name="student" type="persontype"/>

<xs:complexType name="persontype">
    <xs:sequence>
        <xs:element name="firstname" type="xs:string"/>
        <xs:element name="lastname" type="xs:string"/>
    </xs:sequence>
</xs:complexType>
```

If there are elements which contains text only (text and attribute):

```
<xs:element name="shoesize">
    <xs:complexType>
        <xs:simpleContent>
            <xs:extension base="xs:integer">
                <xs:attribute name="country" type="xs:string" />
            </xs:extension>
        </xs:simpleContent>
    </xs:complexType>
</xs:element>
```
or

```
<xs:element name="shoesize" type="shoetype"/>

<xs:complexType name="shoetype">
    <xs:simpleContent>
        <xs:extension base="xs:integer">
            <xs:attribute name="country" type="xs:string" />
        </xs:extension>
    </xs:simpleContent>
</xs:complexType>
```

If there is the need of containing mixed elements:

```
<xs:element name="letter">
    <xs:complexType mixed="true">
        <xs:sequence>
            <xs:element name="name" type="xs:string"/>
            <xs:element name="orderid" type="xs:positiveInteger"/>
            <xs:element name="shipdate" type="xs:date"/>
        </xs:sequence>
    </xs:complexType>
</xs:element>
```

