# Programmazione Web

Contiene gli appunti delle lezioni di programmazione web.

> @author/s: Giovanni Foletto
> @date: 2023-03-28

## Structure

- `README.md` file
- `01_* files`: contains the notes
- `create_pdf.sh`: script to create the pdf.
- `all.pdf`: all notes put toghether and printed in a pdf.


